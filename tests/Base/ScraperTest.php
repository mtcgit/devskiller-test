<?php

///Basis: https://www.geeksforgeeks.org/phpunit-assertequals-function/

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class ScraperTest extends TestCase
{
    /**
     * @test
     */
    public function testGetWordsFromUrl()
    {
        $scraper = new Scraper('tests/Base/test.txt');
        $scraper->getMostFrequentWords();
        $elems = $scraper->getWords();
        sort($elems);

        $expectedResult = ["Bergmann", "Bergmann", "Bergmann", "PHPUnit", "PHPUnit", "PHPUnit", "Sebastian", "Sebastian", "Sebastian", "and", "and", "by", "by", "contributors", "contributors"];

        $this->assertIsArray($elems);
        $this->assertEquals($expectedResult, $elems);



        //Answer///////////////////
         $counted = array_count_values($expectedResult);
          arsort($counted);
          return(key($counted));

          // Result: PHPUnit  by Sebastian Bergmann and contributors

    }


    /**
     * @test
     */
    public function testWordsDict()
    {
        $scraper = new Scraper('tests/Base/test.txt');
        $elems = $scraper->getMostFrequentWords();

        $expectedResult = ["bergmann" => 3, "phpunit" => 3, "sebastian" => 3, "and" => 2, "by" => 2, "contributors" => 2];

        // then
        $this->assertIsArray($elems);
        $this->assertEquals(array_values($expectedResult), array_values($elems));
        $this->assertEquals($expectedResult, $elems);


        //Answer///////////////////
        echo (str_word_count($expectedResult,3));
        echo (str_word_count($expectedResult,2));

        //Either/////

        arsort($expectedResult);
        // Result: PHPUnit   Sebastian Bergmann and contributors.



    }


    /**
     * @test
     */
    public function testWordsDictWithLimit()
    {
        $scraper = new Scraper('tests/Base/test.txt');
        $elems = $scraper->getMostFrequentWords(3);

        $expectedResult = ["bergmann" => 3, "phpunit" => 3, "sebastian" => 3];

        // then
        $this->assertIsArray($elems);
        $this->assertEquals($expectedResult, $elems);


        //Answer///////////////////
        $words = array_count_values($expectedResult); // Count the number of occurrence
        arsort($words); // Sort based on count
        return array_slice($words, 0, 3);

        // Result: PHPUnit  by Sebastian Bergmann 

    }
}
