<?php

declare(strict_types=1);

class Scraper
{
    private $words = [];
    private $results = [];
    private $urlForParsing;

    public function __construct(string $fileName)
    {
        $this->urlForParsing = $fileName;
    }

    public function getUrlForParsing(): string
    {
        return $this->urlForParsing;
    }

    public function getWords(): array
    {
        return $this->words;
    }

    public function getResults(): array
    {
        return $this->results;
    }

    public function getMostFrequentWords(int $count = 100): array
    {
        // implement
        return [];
    }

    private function getWordsFromUrl(): void
    {
        // implement
    }

    private function calculateCountWords(): void
    {
        // implement
    }
}
