# PHP Web Scraper

## Introduction

A compact customizable web scraper that reads contents of a page (from an URL or from a local file), scrapes the page and extracts the complete vocabulary of the document.

## Task details

Logic Flow
1. Get contents of the page based on a passed filename or URL. 
2. Extract the contents of the page (ignore all HTML elements, tags etc.)
3. Use a regular expression to extract all of the words. A word is any string that consists of lower and uppercase letters. (Implement `getWordsFromUrl()` function)
4. Create a frequency count of all extracted words. Letters case should be ignored when counting. (Implement `calculateCountWords()` function)
5. Return an array of words and counts with top `$count` most frequent words, where the lower-cased word is a key and the count is a value. Items in the resulting array should be sorted by frequency, the order of words with the same frequency is not important. (Implement `getCountWords`)


## Sample usage

```php

$obj = new Scraper('http://php.net/manual/en/function.file-get-contents.php');
// OR
$obj = new Scraper('test.txt');


var_dump($obj->getCountWords());

// Sample output:
// array(2) {
//     ["bergmann"]=>
//     int(3)
//     ["phpunit"]=>
//     int(3)
// }
```

## Hints

You shouldn't modify the unit tests, just complete files.
